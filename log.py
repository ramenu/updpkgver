
from sys import stderr
from color import Color
    
def notify(msg : str, color=Color.GREEN):
    print(f"{color}{Color.BOLD}==>{Color.ENDC} {Color.BOLD}{msg}{Color.ENDC}")

def notify_sub(msg : str, color=Color.BLUE):
    print(f"{color}{Color.BOLD}  ->{Color.ENDC} {msg}")

def warning(msg : str):
    print(f"{Color.BROWN}{Color.BOLD}warning{Color.ENDC}{Color.BOLD}:{Color.ENDC} {msg}")

def eprint(msg):
    print(f"{Color.RED}{Color.BOLD}error{Color.ENDC}{Color.BOLD}:{Color.ENDC} {msg}", file=stderr)

def success(msg):
    print(f"{Color.GREEN}{Color.BOLD}{msg}{Color.ENDC}")
