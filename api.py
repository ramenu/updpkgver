import re
import log
import urllib.request
import json
from datetime import datetime
from srcinfo import SOURCES

GITHUB_URL = r'https?://(?:www\.)?github\.com'
GITHUB_PATH_RE = r'/([^\s\/]*)/([^\s\/]*)'
GITHUB_URL_RE = re.compile(GITHUB_URL)

# Some repositories don't actively update the Github
# releases page and instead just tag the commits. This
# set contains 'username:repository' matches to such repositories.
TAGGED_REPOS = {
    'evilsocket:opensnitch'
}

# Replace this URL with whatever repository you host your
# packages on
PKGS_REPO_URL = f'https://api.github.com/repos/Ramenu/rm-extra'

# For converting datetime formats
GITHUB_TIME_FORMAT = '%Y-%m-%dT%H:%M:%SZ'

def fetch_json(url : str, token : str) -> dict:
    try:
        if token == "":
            req = urllib.request.Request(url)
        else:
            headers = { 'Authorization' : f'token {token}' }
            req = urllib.request.Request(url, headers=headers)
        apiJson = json.loads(urllib.request.urlopen(req).read())
        return apiJson
    except urllib.error.HTTPError as err:
        log.eprint(f"failed to fetch JSON from '{url}'. Received HTTP status code {err.status}")
        exit(1)

def find_source_api(srcinfo : dict[str, list[str]]):
    if GITHUB_URL_RE.match(srcinfo['url']):
        return srcinfo['url']
    
    def is_source_valid(source : str) -> (str, bool):
            pattern = re.compile(GITHUB_URL + GITHUB_PATH_RE).search(source)
            if pattern is not None:
                return pattern.group(0), True
            return "", False

    for sourceField in SOURCES:
        if sourceField not in srcinfo:
            continue

        if type(srcinfo[sourceField]) is not list:
            apiUrl, valid = is_source_valid(srcinfo[sourceField])
            if valid:
                return apiUrl
        else:
            for source in srcinfo[sourceField]:
                apiUrl, valid = is_source_valid(source)
                if valid:
                    return apiUrl

    log.eprint(f"failed to find a compatible API for '{srcinfo['url']}'.")
    exit(1)

def fetch_latest_from_github(source : str, pkgver : str, token : str, srcinfo : dict[str, list[str]], isGitPkg = False) -> tuple[str, bool]:
    pattern = re.match('^' + GITHUB_URL + GITHUB_PATH_RE, source)

    if pattern is None:
        log.eprint(f"'failed to fetch API from {source}'. Invalid URL")
        exit(1)

    owner = pattern.group(1)
    repo = pattern.group(2)
    
    if isGitPkg:
        url = f'https://api.github.com/repos/{owner}/{repo}'
        apiJson = fetch_json(url, token)
        pushedAt = datetime.strptime(apiJson['pushed_at'], GITHUB_TIME_FORMAT)
        pkgRepoJson = fetch_json(PKGS_REPO_URL, token)
        pkgRepoPushedAt = datetime.strptime(pkgRepoJson['pushed_at'], GITHUB_TIME_FORMAT)
        # String is empty since updpkgver does not update Git packages
        return "", pushedAt > pkgRepoPushedAt

    ownerRepo = f"{owner}:{repo}"
    if ownerRepo in TAGGED_REPOS:
        url = f"https://api.github.com/repos/{owner}/{repo}/tags"
        log.notify(f"Fetching JSON from '{url}'...")
        apiJson = fetch_json(url, token)
        for tag in apiJson:
            tagName = tag['name']

            # Skip release candidates
            if 'rc' in tagName:
                continue

            if pkgver == tagName:
                return "", False
            return tagName, True
        return "", False
            

    url = f'https://api.github.com/repos/{owner}/{repo}/releases/latest'
    log.notify(f"Fetching JSON from {url}")

    apiJson = fetch_json(url, token)

    latestTagName = apiJson['tag_name']

    if latestTagName.startswith('v'):
        latestTagName = latestTagName[1:]
    if pkgver == latestTagName:
        return "", False
        
    return latestTagName, True



def fetch_latest_version(pkgver : str, token : str, srcinfo : dict[str, list[str]], isGitPkg = False) -> tuple[str, bool]:
    source = find_source_api(srcinfo)
    if GITHUB_URL_RE.match(source):
        newpkgver, updated = fetch_latest_from_github(source, pkgver, token, srcinfo, isGitPkg)
        newpkgver = newpkgver.split('-')[0]
        return newpkgver, updated

    log.warning(f"could not find a compatible API for '{source}', skipping..")
    return "", False

            
