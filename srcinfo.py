import os
import sys
import log

SRCINFO = '.SRCINFO'
VALID_FIELDS = {
    'pkgver',
    'pkgrel',
    'epoch',
    'pkgdesc',
    'url',
    'install',
    'changelog',
    'arch',
    'groups',
    'license',
    'noextract',
    'options',
    'backup',
    'validpgpkeys',
    'source',
    'source_x86_64',
    'source_i686',
    'source_pentium4',
    'source_armv7h',
    'source_aarch64',
    'depends_x86_64',
    'depends_i686',
    'depends_pentium4',
    'depends_armv7h',
    'depends_aarch64',
    'checkdepends_x86_64',
    'checkdepends_i686',
    'checkdepends_pentium4',
    'checkdepends_armv7h',
    'checkdepends_aarch64',
    'makedepends_x86_64',
    'makedepends_i686',
    'makedepends_pentium4',
    'makedepends_armv7h',
    'makedepends_aarch64',
    'optdepends_x86_64',
    'optdepends_i686',
    'optdepends_pentium4',
    'optdepends_armv7h',
    'optdepends_aarch64',
    'provides_x86_64',
    'provides_i686',
    'provides_pentium4',
    'provides_armv7h',
    'provides_aarch64',
    'conflicts_x86_64',
    'conflicts_i686',
    'conflicts_pentium4',
    'conflicts_armv7h',
    'conflicts_aarch64',
    'replaces_x86_64',
    'replaces_i686',
    'replaces_pentium4',
    'replaces_armv7h',
    'replaces_aarch64',
    'md5sums_x86_64',
    'md5sums_i686',
    'md5sums_pentium4',
    'md5sums_armv7h',
    'md5sums_aarch64',
    'sha1sums_x86_64',
    'sha1sums_i686',
    'sha1sums_pentium4',
    'sha1sums_armv7h',
    'sha1sums_aarch64',
    'sha224sums_x86_64',
    'sha224sums_i686',
    'sha224sums_pentium4',
    'sha224sums_armv7h',
    'sha224sums_aarch64',
    'depends',
    'checkdepends',
    'makedepends',
    'optdepends',
    'provides',
    'conflicts',
    'replaces',
    'cksums',
    'md5sums',
    'sha1sums',
    'sha256sums',
    'sha224sums',
    'sha384sums',
    'sha512sums',
    'b2sums',
    'pkgname',
    'pkgbase',
    'sha256sums_x86_64',
    'sha256sums_i686',
    'sha256sums_pentium4',
    'sha256sums_armv7h',
    'sha256sums_aarch64',
    'sha384sums_x86_64',
    'sha384sums_i686',
    'sha384sums_pentium4',
    'sha384sums_armv7h',
    'sha384sums_aarch64',
    'sha512sums_x86_64',
    'sha512sums_i686',
    'sha512sums_pentium4',
    'sha512sums_armv7h',
    'sha512sums_aarch64'
}

SOURCES = {
    'source',
    'source_x86_64',
    'source_i686',
    'source_pentium4',
    'source_armv7h',
    'source_aarch64'
}


def parse_srcinfo(srcinfoPath : str) -> dict[str, list[str]]:
    table = {}
    if not os.path.isfile(srcinfoPath):
        log.eprint(f"'{srcinfoPath}' is not a file")
        exit(1)

    try:
        if os.path.getsize(srcinfoPath) == 0:
            raise Exception(f"'{srcinfoPath}' is empty")
        with open(srcinfoPath) as file:
            for line in file.readlines():
                fields = line.split('=')

                if not len(fields) == 2:
                    continue
                
                key = fields[0].strip()
                value = fields[1].strip()

                if key not in VALID_FIELDS:
                    log.eprint(f"failed to parse '{srcinfoPath}': unknown field '{key}'")
                    exit(1)

                if not key in table:
                    table[key] = value
                else:
                    if type(table[key]) is not list:
                        table[key] = [table[key], value]
                    else:
                        table[key] += value
    except Exception as err:
        log.eprint(f"{err}")
        exit(1)

    return table

        
